const { createPool } = require("mysql");

const conf = createPool({
    port: process.env.PORT_DB,
    host: process.env.HOST_DB,
    user: process.env.USER_DB,
    password: process.env.PASS_DB,
    database: process.env.DB_NAME,
    connectionLimit: 10
})

module.exports = conf;