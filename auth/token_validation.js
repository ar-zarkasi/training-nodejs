const { verify } = require("jsonwebtoken");

module.exports = {
    checkToken: (req, res, next) => {
        let token = req.get('authorization');
        if (token) {
            token = token.slice(7);
            verify(token, process.env.TOKEN, (err, decoded) => {
                if (err) {
                    res.status(401).json({
                        success: false,
                        message: "Invalid Authorization"
                    })
                } else {
                    next();
                }
            })
        } else {
            res.status(401).json({
                success: false,
                message: "Access Denied unauthorized User"
            });
        }
    }
}