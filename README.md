pastikan sudah memiliki database terlebih dahulu, lalu lakukan
---------------
npm install
---------------
terlebih dahulu lalu buat file ".env" yang berisi konfigurasi:
-----------------
PORT_SERVER=3000
HOST_DB="localhost atau hostname sesuai settingan anda"
PORT_DB="default 3306"
USER_DB="user db"
DB_NAME="nama db"
PASS_DB="password db"
TOKEN="Terserah yang penting ada"
-----------------

running dengan perintah:
---------------------------
npm run start