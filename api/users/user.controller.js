const { create, getUsers, getUsersById } = require('./user.service');
const { genSaltSync, hashSync, compareSync } = require("bcrypt")
const { sign } = require("jsonwebtoken");

module.exports = {
    createUser: (req, res) => {
        const body = req.body;
        const salt = genSaltSync(10)
        body.password = hashSync(body.password, salt);
        create(body, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: false,
                    message: "cannot create user",
                });
            }
            return res.status(200).json({
                success: true,
                data: results
            });
        })
    },
    getUsers: (req, res) => {
        getUsers((err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: false,
                    message: 'An Error Occured'
                });
            }
            return res.status(200).json({
                success: true,
                data: results
            })
        })
    },
    getUsersById: (req, res) => {
        const id = req.params.id;
        getUsersById(id, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: false,
                    message: "error occured"
                });
            }
            if (!results) {
                return res.status(404).json({
                    success: false,
                    message: "Record Not Found"
                });
            }
            return res.status(200).json({
                success: true,
                detail: results[0]
            });
        });
    },
    login: (req, res) => {
        const body = req.body;
        getUsersById(body.id, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: false,
                    message: "error occured"
                });
            }
            if (!results) {
                return res.status(404).json({
                    success: false,
                    message: "Users Not Found"
                });
            }
            const user = results[0];
            const sama = compareSync(body.password, user.password)
            if (sama) {
                user.password = undefined;
                const jsontoken = sign({ result: user }, process.env.TOKEN, { expiresIn: '1h' });
                user.token = jsontoken;
                return res.status(200).json({
                    success: true,
                    detail: user
                });    
            } else {
                return res.status(200).json({
                    success: false,
                    message: "Wrong Password"
                })
            }
            
        });
    }
}
