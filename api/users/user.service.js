const db = require("../../config/database");

module.exports = {
    create: (data, callback) => {
        db.query(`INSERT INTO users(name, idgoogle, picture, password) VALUES(?,?,?,?)`, [
            data.name,
            data.idgoogle,
            data.picture,
            data.password
        ],
            (error, results, fields) => {
                if (error) {
                    return callback(error);
                }
                return callback(null, results);
            }
        );
    },
    getUsers: callback => {
        db.query(`SELECT * FROM users`,
            [],
            (error, results, fields) => {
                if (error) {
                    return callback(error);
                }
                return callback(null, results);
            }
        )
    },
    getUsersById: (id, callback) => { 
        db.query(`SELECT *FROM users WHERE idgoogle = ?`,
            [id],
            (error, results, fields) => {
                if (error) {
                    return callback(error);
                }
                return callback(null, results);
            }
        );
    },
};