require("dotenv").config();
const express = require("express");
const app = express();
const cors = require('cors');
const portserver = process.env.PORT_SERVER;
const userRouter = require("./api/users/user.router");
app.use(express.json());
app.use(cors({
    origin: '*'
}))

app.get("/", (req, res) => {
    res.json({
        success: 'ok',
        message: 'Restfull API running'
    })
})

app.use("/users", userRouter);

app.listen(portserver, () => {
    console.log('Server Restfull API Running on ' + portserver);
})